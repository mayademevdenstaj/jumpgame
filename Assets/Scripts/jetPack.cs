﻿using UnityEngine;
using System.Collections;
using EZ_Pooling;

public class jetPack : MonoBehaviour {

	Player player;
    public bool isCombo;

	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player" && !GameManager.gameOver) {
			player = col.gameObject.GetComponent<Player> ();
			int sec = PlayerPrefs.GetInt ("jetpackSec");

            if (isCombo && player.headstarted)
            {
                player.jetpackTotalFuel += sec;
                player.enableShield(player.shieldStateCounter + sec);
            }
            else if (!player.headstarted)
            {
                player.jetpackTotalFuel = sec;
                player.enableShield(sec);
            }

			player.jetpackPickup ();
			EZ_PoolManager.Despawn (transform);
		}
	}
}
