﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour {
    public Text scoreText;
    public Text coinText;
    public Text coinMultiplierText;
    public Text dieCoinText;
    public Text dieScoreText;
    public Text totalCoinText;
    public Text highScoreText;
	public AudioSource fallaudio;
	public AudioSource falledAudio;
	public AudioMixer audioMix;
    public Image reviveHead;
    public Text keyCount;
    bool revived = false;
    public GameObject headstarts;
    public Button headstart100Button;
    public Button headstart250Button;
	public bool revieveScreen;
	public Player player;
	public static bool gameOver;
	public static bool gameStarted;
	public Button shop;
	public Button characterShop;
	public GameObject buyReviveScreen;
	public GameObject notEnoughCoin;
	public GameObject notEnoughCoinHeadstart;
	public GameObject pauseScreen;
	public GameObject buyHeadStartScreen;

    public int score; //bu oyunun scoru
    int totalCoin;
	public float memCoinPitch;
	int dontChangedFrame;// coinlerin pitch'i nin kaç frame dir değişmediğidir.
	public static int coinEarned;
    public static int coinMultiplier;  //zamanla artıyor ve her coinin değerini arttırıyor

	// Use this for initialization
	void Start () {
        gameStarted = false;
        gameOver = false;
        coinEarned = 0;
        coinMultiplier = coinMultiplierCalculater(PlayerPrefs.GetInt("totalScore"));  //oyuncunun bütün scoreları toplamı "totalScore"
    }
	
	void Update () {
		if (getCoinPitch() != memCoinPitch) {
			dontChangedFrame = 0;
		} else {
			dontChangedFrame++;
		}

		if (dontChangedFrame > 20 && getCoinPitch () > 1f)
			audioMix.SetFloat ("coinPitch", 1);

		memCoinPitch = getCoinPitch ();

		if (gameStarted)
        {
            score = (int)Camera.main.transform.position.y;
            UIupdate();
        }

        if (Camera.main.transform.position.y > 5)
            headstarts.SetActive(false);

	}

    void UIupdate()
    // Oyun içi menüdeki UI'ı güncelliyor
    {
        scoreText.text = score.ToString()+"m";
        coinText.text = coinEarned.ToString();
        coinMultiplierText.text = coinMultiplier.ToString();
    }

    int coinMultiplierCalculater(int totalScore)
    // Oyuncunun bütün scoreları toplamını alarak coinMultiplier'ı bulur. 2nin üsleri * 2000 score'da bir multiplier artar
    {
        int multiplier = 1;
        int count = 1;
        while (multiplier < totalScore / 2000)
        {
            if (count < 2000)
                count++;
            else
                break;
            multiplier *= 2;
        }
        PlayerPrefs.SetInt("coinMultiplier", count);
        return count;
    }

    public void restart()
    {
		SceneManager.LoadScene(0);
    }

    public void gameEnded()
    {
		revieveScreen = true;
        revived = false;
        gameOver = true;
        if (score > PlayerPrefs.GetInt("highScore"))
            PlayerPrefs.SetInt("highScore", score);
        PlayerPrefs.SetInt("totalScore", PlayerPrefs.GetInt("totalScore") + score);
        PlayerPrefs.SetInt("totalCoin", PlayerPrefs.GetInt("totalCoin") + coinEarned);

        dieCoinText.text = coinEarned.ToString();
        dieScoreText.text = score.ToString();
        totalCoinText.text = PlayerPrefs.GetInt("totalCoin").ToString();
		highScoreText.text = "Best\n"+PlayerPrefs.GetInt("highScore").ToString();

        keyCount.text = PlayerPrefs.GetInt("reviveKey").ToString();
		fallaudio.Play ();
        reviveScreen();
    }

    public void reviveScreen()
    {
        
        if (!revived)
        {
            if (reviveHead.fillAmount != 0)
            {
				reviveHead.fillAmount -= Time.deltaTime/5;
                Invoke("reviveScreen", 0.015f);
            }
            else
            {
				revieveScreen = false;
				fallaudio.Stop();
				falledAudio.Play();
            }
        }
    }

    public void revive()
    {
        if (PlayerPrefs.GetInt("reviveKey") > 1)
        {
            PlayerPrefs.SetInt("reviveKey", PlayerPrefs.GetInt("reviveKey") - 2);
            revived = true;
			revieveScreen = false;
            reviveHead.fillAmount = 360;
            GameManager.gameOver = false;
            player.jetpackTotalFuel += 3;
            player.enableShield(3.5f);
            player.jetpackPickup();
            player.transform.position = new Vector3(0, Camera.main.transform.position.y - 3.9f, 0);
            player.characterSprite.transform.localScale = new Vector3(player.transform.localScale.x, 1, 1);
            fallaudio.Stop();
        }
        else {
			buyReviveScreen.SetActive (true);
			Time.timeScale = 0f;
        }
    }

	public void buyHeadStart100()
	{
		if (PlayerPrefs.GetInt ("totalCoin") >= 2000) {
			PlayerPrefs.SetInt ("headStart100", PlayerPrefs.GetInt ("headStart100") + 1);
			PlayerPrefs.SetInt ("totalCoin", PlayerPrefs.GetInt ("totalCoin") - 2000);
		}else 
			notEnoughCoinHeadstart.SetActive (true);
	}

	public void buyHeadStart250()
	{
		if (PlayerPrefs.GetInt ("totalCoin") >= 4000) {
			PlayerPrefs.SetInt ("headStart250", PlayerPrefs.GetInt ("headStart250") + 1);
			PlayerPrefs.SetInt ("totalCoin", PlayerPrefs.GetInt ("totalCoin") - 4000);
		}else 
			notEnoughCoinHeadstart.SetActive (true);
	}

	public void buyRevive()
	{
		if (PlayerPrefs.GetInt ("totalCoin") >= 3000) {
			PlayerPrefs.SetInt ("totalCoin", PlayerPrefs.GetInt ("totalCoin") - 3000);
			PlayerPrefs.SetInt ("reviveKey", PlayerPrefs.GetInt ("reviveKey") + 1);
			keyCount.text = PlayerPrefs.GetInt("reviveKey").ToString();
		}
		else 
			notEnoughCoin.SetActive (true);
	}

	public void backFromBuyRevive()
	{
		buyReviveScreen.SetActive (false);
		Time.timeScale = 1f;
	}

	public float getCoinPitch(){
		float value;
		bool result =  audioMix.GetFloat("coinPitch", out value);
		if(result){
			return value;
		}
        else{
			return 0f;
		}
	}

    public void headstart100()
    {
		if (PlayerPrefs.GetInt ("headStart100") > 0) {
			player.jetpackTotalFuel += 0.2f;
			player.enableShield (5.1f);
			player.jetpackPickup ();
			player.jumpSpeed = player.jumpSpeed * 3;
			PlayerPrefs.SetInt ("headStart100", PlayerPrefs.GetInt ("headStart100") - 1);
			player.headstarted = true;
		}
		else {
			Time.timeScale = 0f;
			buyHeadStartScreen.SetActive (true);
			pauseScreen.SetActive (true);
		}
    }

    public void headstart250()
    {
		if (PlayerPrefs.GetInt ("headStart250") > 0) {
			player.jetpackTotalFuel += 3f;
			player.enableShield (7.8f);
			player.jetpackPickup ();
			player.jumpSpeed = player.jumpSpeed * 3;
			PlayerPrefs.SetInt ("headStart250", PlayerPrefs.GetInt ("headStart250") - 1);
			player.headstarted = true;
		} 
		else {
			Time.timeScale = 0f;
			buyHeadStartScreen.SetActive (true);
			pauseScreen.SetActive (true);
		}
    }

	public void tap(){
		if (!gameStarted) {
			gameStarted = true;
			player.onGameStart ();
			shop.enabled = false;
			characterShop.enabled = false;
		}
	}
}
